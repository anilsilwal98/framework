<?php

require_once __DIR__.'/../vendor/autoload.php';

use Bidhee\Simplex\Framework;
use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

function  render_template($request)
{
    extract($request->attributes->all(),EXTR_SKIP);
    ob_start();
    include sprintf(__DIR__.'/../src/pages/%s.php', $_route);

    return new Response(ob_get_clean());
}



//takes the request from clients;
$request = Request::createFromGlobals();
//$response = new Response();
$routes = include  __DIR__.'/../src/app.php';

$context = new RequestContext();
$context->fromRequest($request);
$matcher = new UrlMatcher($routes,$context);
//$resolver = new HttpKernel\Controller\ControllerResolver();
$resolver = new ControllerResolver();

$framework = new Framework($matcher, $resolver);

$response = $framework->handle($request);

//try {
//    $request->attributes->add($matcher->match($request->getPathInfo()));
//
//    $controller = $resolver->getController($request);
//    $arguments = $resolver->getArguments($request,$controller);
//    $response = call_user_func_array($controller, $arguments);
//
////    $response = call_user_func($request->attributes->get('_controller'),$request);
//
//}catch(Routing\Exception\ResourceNotFoundException $e) {
//    $response = new Response('Not Found', 404);
//}catch(Exception $e){
//    $response = new Response('An error occured',500);
//}

$response->send();
