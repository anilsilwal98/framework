<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();


$routes->add('is_leap_year', new Routing\Route('/is_leap_year/{year}',
    [
        'year' => null,
        '_controller' => 'Bidhee\\Calender\\Controller\\LeapYearController::indexAction',
    ]
));

$routes->add('signupForm', new Routing\Route('/signupForm/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::signupForm',
]));

$routes->add('signup', new Routing\Route('/signup/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::signUp',
]));

$routes->add('loginForm', new Routing\Route('/loginForm/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::loginForm',
]));

$routes->add('login', new Routing\Route('/login/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::login',
]));

$routes->add('dashboard', new Routing\Route('/login/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::welcome',
]));

$routes->add('logout', new Routing\Route('/logout/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::logout',
]));

$routes->add('testing', new Routing\Route('/testing/{request}', [
    'request' => null,
    '_controller' => 'Bidhee\\Calender\\Controller\\UserController::testing',
]));


return $routes;


//
//$routes->add('hello', new Routing\Route('/hello/{name}',array(
//    'name'=>'World',
//     '_controller'=> function($request){
//          $response = render_template($request);
//          return $response;
//     })));
//$routes->add('bye', new Routing\Route('/bye'));

