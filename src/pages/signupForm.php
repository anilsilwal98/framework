<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign UP</title>
</head>
<link href="/../framework/web/dist/css/bootstrap.css" rel="stylesheet">
<link href="/../framework/web/dist/css/jquery-ui.css" rel="stylesheet">

<script src="/../framework/web/dist/js/jquery-1.10.2.js"></script>
<script src="/../framework/web/dist/js/jquery-ui.js"></script>

<script>
    $(function () {
        $("#datepicker").datepicker();
    });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<div class="container">
    <div class="row">
        <div style="position: relative;left:350px;top:140px;"
             class="col-xs-12 col-sm-12 col-md-4 well well-sm">
            <legend><a href="http://www.jquery2dotnet.com"><i class="glyphicon glyphicon-globe"></i>
                </a> Yarsha Entry!
            </legend>
            <form action="signup" method="post" class="form" name="signup" role="form">
                <input class="form-control" name="name" placeholder="Full Name" type="text"/>
<!--                <input type="hidden" name="token" value="--><?php //echo $token; ?><!--" />-->

                <input class="form-control" name="email" placeholder="Your Email" ng-model="email" type="email"/>

                <input class="form-control" id="txtpassword" name="password" placeholder="New Password"
                       type="password"/>
                <br/>
                <p>Date: <input type="datetime" name="date" id="datepicker"></p>
                <br/>
                <div class="g-recaptcha" data-sitekey="6LfdshgTAAAAAPld2GzQ_empho35OpaPgKW9Wgnt"></div>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Sign up
                </button>
            </form>
        </div>
    </div>
</div>
</body>

</html>
