<?php

namespace Bidhee\Calender\Controller;

use Symfony\Component\HttpKernel;
use Bidhee\Calender\Model\LeapYear;
use Symfony\Component\HttpFoundation\Response;
class LeapYearController
{
    public function indexAction($year)
    {
        $leapYear = new LeapYear();
        if($leapYear->is_leap_year($year))
        {
            return new Response('Yep, Its a leap year');
        }

        return new Response('Nope, this is  not a leap year');
    }
}
