<?php

namespace Bidhee\Calender\Foundation;

interface ServiceProviderInterface
{

    /**
     * Registers a Service into the Container
     *
     * @return void
     */
    public function register();

}
