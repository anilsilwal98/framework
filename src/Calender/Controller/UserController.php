<?php


namespace Bidhee\Calender\Controller;

require_once __DIR__ . '/../../../vendor/autoload.php';

use Bidhee\Calender\Provider\UserServiceProvider;
use Bidhee\Calender\Repository\UserRepository;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\HttpKernel;
use Doctrine\ORM\EntityManager;
use Bidhee\Calender\Model\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bidhee\Calender\Controller\MailMail;
use Illuminate\Container;

class UserController extends AbstractController
{

    public function signupForm($request)
    {
        $routes = include __DIR__ . '/../../pages/signupForm.php';

        return new Response($routes);

    }

    public function signUp($request)
    {
        $name = $_POST["name"];
        // gets POST VAR    syntax $request->request->get('PARAMS');
        // gets GET VAR syntax $request->query->get('PARAMS');

//        $name = $request->request->get('name');
//        $email = $request->request->get('email');
//        $password = $request->request->get('password');
//        $date = $request->request->get('date');

        $email = $_POST["email"];
        $password = $_POST["password"];
        $date = $_POST["date"];

        $date = new \DateTime($date);
        $createdAt = new \DateTime();
        $updatedAt = new \DateTime();

        $user = new User();
        $user->setName($name);
        $user->setPassword($password);
        $user->setEmail($email);

        $user->setDob($date);
        $user->setCreatedAt($createdAt);
        $user->setUpdatedAt($updatedAt);

        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception $e) {
            echo $e;
        }
        echo "ID" . $user->getIdUser() . "\n";
        $token = md5(uniqid(rand(), true));

        $routes = new MailMail();
        $routes->send($email, $token);

        return new Response('Please check your mail address to activate the account');

    }

    public function loginForm($request)
    {
        $routes = include __DIR__ . '/../../pages/loginForm.php';

        return new Response($routes);

    }

    /**
     * @param $request
     * @return Response
     */
    public function login($request)
    {

        session_start();

        $email = $_POST["email"];
        $password = $_POST["password"];

//        $entityManager = require __DIR__ . '/../../../config/bootstrap.php';
//        $entityManager = $this->getEntity();

        /** @var User $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findOneBy(['email' => $email]);
        if ($user) {
            //echo "Email is Registered!\n";
            //echo $user->getEmail();
            if (password_verify($password, $user->getPassword()) == true) {

                $_SESSION['user_id'] = $user->getIdUser();
                $routes = include __DIR__ . '/../../pages/welcome.php';

                return new Response($routes);

            } else {
                return new Response('Password incorrect');
            }
        } else {
            return new Response('Not Registered yet');

        }
    }

    public function welcome($request)
    {
        session_start();

        if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {
            $routes = include __DIR__ . '/../../pages/loginForm.php';
        } else {
            $routes = include __DIR__ . '/../../pages/welcome.php';
        }

        return new Response($routes);

    }

    public function logout($request)
    {
        session_start();
        session_destroy();
        $routes = include __DIR__ . '/../../pages/loginForm.php';

        return new Response($routes);

    }
}
