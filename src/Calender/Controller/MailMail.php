<?php
namespace Bidhee\Calender\Controller;

require __DIR__ . '/../../../vendor/autoload.php';

use Swift_Message;
use Swift_Mailer;
use Swift_MailTransport;

class MailMail{
    protected  $email;
    protected $token;
    public function __construct()
    {

    }

    public function send($email,$token)
    {
        $transport = Swift_MailTransport::newInstance();
        $message = Swift_Message::newInstance();
        $message
            ->setTo(array($email))
            ->setSubject("Activate your account")
            ->setBody(
                '<body>'.
                'Please click below link to activate your yarsha account'.
                '<br/>'.
                '<a href = "">"<?php echo $token ?>"</a>'.
                '<br/>'.
                'stay in touch with us'.
                '</body>',
                'text/html')
            ->setFrom("anilsilwal98@gmail.com", "anil");

        $mailer = Swift_Mailer::newInstance($transport);
        $mailer->send($message);

    }
}
