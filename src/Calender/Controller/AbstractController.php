<?php
/**
 * Created by PhpStorm.
 * User: anil
 * Date: 2/23/16
 * Time: 4:27 PM
 */

namespace Bidhee\Calender\Controller;


use Doctrine\ORM\EntityManager;

abstract class AbstractController
{

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct()
    {
        $app = require __DIR__ . '/../../../bootstrap.php';
        $this->em = $app->run()->getContainer()['em'];
    }

}
