
<?php

namespace  Bidhee\Calender\Model;
/**
 * @Entity @Table(name="articles")
 **/
class Article
{

    use Traits\NextTrait;

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="idUser")
     */
    protected $user;

    /**  @Column(name="news_title",type="string",nullable=false, length = 100) */
    protected $news_title;

    /** @Column(name="news_content",nullable=false,type="text",length=400) */
    protected $news_content;

    /** @Column(name="img_name",type="string",length = 100) */
    protected $img_name;

    /**
     * Get newsTitle
     *
     * @return string
     */

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;

    }


    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getnewsTitle()
    {
        return $this->news_title;
    }

    public function setnewsTitle($news_title)
    {
        $this->news_title = $news_title;

        return $this;
    }


    public function getnewsContent()
    {
        return $this->news_content;
    }

    public function setnewsContent($news_content)
    {
        $this->news_content = $news_content;

        return $this;
    }

    /**
     * Get imgName
     *
     * @return string
     */
    public function getimgName()
    {
        return $this->img_name;
    }

    public function setimgName($img_name)
    {
        $this->img_name = $img_name;

        return $this;
    }


}
