<?php
namespace  Bidhee\Calender\Model;

//use Anil\Traits\NextTrait;

/**
 * @Entity @Table(name="users")
 **/
class User
{
    use Traits\NextTrait;

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $idUser;

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /** @Column(name="name",type="string", length = 200) */
    protected $name;

    /** @Column(name="email", type="string",unique=true,nullable=false,length = 200) */
    protected $email;

    /** @Column(name="dob",type="datetime") */
    protected $dob;

    /** @Column(name="password",type="string",length = 200) */
    protected $password;


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get DOB
     *
     * @return datetime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set DOB
     *
     * @param datetime $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        return $this;
    }


}
