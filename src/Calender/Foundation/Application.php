<?php
namespace Bidhee\Calender\Foundation;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

use Illuminate\Contracts\Container\Container;
use Bidhee\Calender\Foundation\Database\DatabaseInterface;
use Bidhee\Calender\Foundation\Database\Database;
use Bidhee\Calender\Provider\DatabaseProvider;
use Bidhee\Calender\Provider\UserServiceProvider;
use Bidhee\Calender\Repository\UserRepositoryInterface;
use Bidhee\Calender\Repository\UserRepository;
use Bidhee\Calender\Foundation\ServiceProviderInterface;

class Application
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function run()
    {
        $this->registerConfig();

        $this->registerServices();

        return $this;

    }

    public function registerConfig()
    {
        $config = require __DIR__ . "/../../../config/config.dist.php";

        $isDevMode = true;
        $conn2 = Setup::createAnnotationMetadataConfiguration([__DIR__ . "/../../../src"], $isDevMode);
        $conn1 = $config['database'];
        $em = EntityManager::create($conn1, $conn2);
//        dump($em);

        // Register the configuration
        $this->container->instance('config', $config);

        $this->container->instance('em', $em);

    }

    public function getContainer()
    {
        return $this->container;
    }

    public function registerServices()
    {
        $providers = $this->container['config']['providers'];
        /** @var ServiceProviderInterface $serviceProvider */
        foreach ($providers as $provider) {
            $serviceProvider = new $provider($this->container);
            $serviceProvider->register();

        }

    }
}
