<?php

namespace Bidhee\Calender\Repository;

interface UserRepositoryInterface
{

    public function getUserById($userId);
}
