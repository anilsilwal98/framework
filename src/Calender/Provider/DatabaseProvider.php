<?php

namespace Bidhee\Calender\Provider;

use Doctrine\DBAL\DriverManager;
use Bidhee\Calender\Foundation\Database\Database;
use Bidhee\Calender\Foundation\Database\DatabaseInterface;
use Bidhee\Calender\Provider\AbstractProvider as Provider;

/**
 * @author Kabir Baidhya
 */
class DatabaseProvider extends Provider
{

    /**
     * Registers a Service into the Container
     *
     * @return void
     */
    public function register()
    {
        // Register DatabaseInterface
        $this->container->bind(DatabaseInterface::class, function ($container) {
            return new Database(
                DriverManager::getConnection($container['config']['database'])
            );
        });

    }
}
