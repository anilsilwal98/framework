<?php


namespace Bidhee\Calender\Provider;

use Bidhee\Calender\Repository\UserRepository;
use Bidhee\Calender\Repository\UserRepositoryInterface;
use Bidhee\Calender\Provider\AbstractProvider as Provider;
use Bidhee\Calender\Foundation\Database\DatabaseInterface;

/**
 * @author Kabir Baidhya
 */
class UserServiceProvider extends Provider
{

    /**
     * Registers a Service into the Container
     *
     * @return void
     */
    public function register()
    {
        // Register UserRepositoryInterface
        $this->container->bind(UserRepositoryInterface::class, function ($container) {
            return new UserRepository($container[DatabaseInterface::class]);
        });
    }
}
