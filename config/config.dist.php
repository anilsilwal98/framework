<?php

return [
    'database' => [
        'driver' => 'pdo_mysql',
        'path' => __DIR__ . '/db.mysql',
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'dbname' => 'project'
    ],
    'providers' => require __DIR__ . '/provider.php'
];
