<?php
use Bidhee\Calender\Foundation\Application;
use Illuminate\Container\Container;

date_default_timezone_set("Asia/Kathmandu");
define('BASE_PATH', __DIR__ . '/');
define('CONFIG_PATH', BASE_PATH . 'config/');
define('CONFIG_FILE', CONFIG_PATH . 'config.php');

// Require Composer's autoloader
require __DIR__ . '/vendor/autoload.php';

// Return a new Application Instance
$app = new Application(new Container());
return $app;
