# A New Framework made using Doctrine and Symfony Components.

### Instruction need to follow to use this framework 
* Type "git clone 'git clone address' to clone this Framework "
* Then type "composer update"
* After that export the database from "sql/.sql" file
* Plus manage your database configuration setting from this file "config/config.dist.php"
* Now you have this framework installed.

Use following routes for user registration and authentication
* signupForm-- for user registration
* loginForm -- for user authentication
 
