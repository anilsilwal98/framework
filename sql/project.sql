-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 23, 2016 at 10:50 AM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `news_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `news_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `img_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `idUser` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `dob` datetime NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUser`, `name`, `email`, `dob`, `password`, `createdAt`, `updatedAt`) VALUES
(2, 'Ashok Silwal', 'asho@yahoo.com', '2016-02-20 00:00:00', '$2y$10$OTtvcKCX4pX5PvKsX1EsnOUCwucbRpGfhqrMqGsW29i6ahlZ08F26', '2016-02-18 15:37:49', '2016-02-18 15:37:49'),
(4, 'Ashok Silwal', 'anil@yahoo.com', '2010-12-12 00:00:00', '$2y$10$vPeuTjUfJD/NVPEc8JNlTO2fZYYcbwrqTRIqfO8fz9qQD/gQ4a8/C', '2016-02-18 17:43:44', '2016-02-18 17:43:44'),
(5, 'usernam', 'user@yahoo.com', '2000-12-12 00:00:00', '$2y$10$UYiaO12GXS1SKZKhJ9xQleQgifcXkwv/EqtOJjwKVT0R2AzgDtQhi', '2016-02-18 17:48:13', '2016-02-18 17:48:13'),
(7, 'Yarsha Company', 'yarshacompany@studio.com', '1992-12-12 00:00:00', '$2y$10$Obi5alE5xvMFS/cj89t6/eFVQkZ5lyYrKENfBBZt2yetSFDz8Ob0O', '2016-02-18 18:00:27', '2016-02-18 18:00:27'),
(8, 'yarsha Company', 'anil@yarsha.com', '1992-12-12 00:00:00', '$2y$10$0j0.Pi23ypK0Qw5bSwnXHO1CpDRlIpP5cVDGEBTi9GKzzeCCMe8PK', '2016-02-18 18:07:03', '2016-02-18 18:07:03'),
(9, 'anil silwal', 'a@yahoo.com', '2010-12-12 00:00:00', '$2y$10$IM8SRMTkzVSpi4DzJdVALeavQjiO9ZLlI64.xNxW2yG6u04Gn4ipW', '2016-02-18 18:35:52', '2016-02-18 18:35:52'),
(10, 'Ashok Hamal', 'hamal@yahoo.com', '1992-02-25 00:00:00', '$2y$10$c4SjwyFIDf1tAWAIFydA6O1T/OJH0IM0IU/OZOQfrneBbfSZfv.jy', '2016-02-19 14:28:42', '2016-02-19 14:28:42'),
(11, 'Ak', 'ak@yahoo.com', '2016-02-01 00:00:00', '$2y$10$UfgNPIrakcMPfb85pvs2xO8l6v9TYW40rH3hhKvXSgaCEywiAQl96', '2016-02-19 14:32:50', '2016-02-19 14:32:50'),
(12, 'wow', 'wow@yahoo.com', '2016-02-01 00:00:00', '$2y$10$k7TIE8WNfLWer6gGfGoZCePsX.Jnk.wQzWccb5NZLenKHcqO16/6S', '2016-02-19 16:18:35', '2016-02-19 16:18:35'),
(18, 'Anil Silwal', 'anilsilwal98+1@gmail.com', '2016-02-25 00:00:00', '$2y$10$95fXGZGCCYx4f9NTreXSWefc6nO7rFV1ygdR8RpShIvWdadGXyDvK', '2016-02-19 18:29:46', '2016-02-19 18:29:46'),
(19, 'Ashik Silwal', 'ashiksilwal@yahoo.com', '1992-02-18 00:00:00', '$2y$10$yFbbxtfXa..n5tQs/ChPVuq4UQ6k5nsKrra7EELumWapeyy5k1j3e', '2016-02-23 16:22:42', '2016-02-23 16:22:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BFDD3168A76ED395` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `FK_BFDD3168A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`idUser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
